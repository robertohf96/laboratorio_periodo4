`include "alu_func.vh"

module alu(
	input [31:0]rd1,
	input [31:0]rd2,
	input [3:0]func,
	output reg isZero,	
	output reg [31:0]result
	);	

	always @ (func or rd1 or rd2) begin
		case(func)
			`ALU_ADD: 
				result = $signed(rd1) + $signed(rd2);
			
			`ALU_SUB: 
				result = $signed(rd1) - $signed(rd2);
			
			`ALU_AND: 
				result = $signed(rd1) & $signed(rd2);

			`ALU_OR: 
				result = $signed(rd1) | $signed(rd2);

			`ALU_SLT: 
				result = {31'd0, $signed(rd1) < $signed(rd2)};

			`ALU_SLL: 
				result = $signed(rd1) << $signed(rd2);
				
			default:
				result = 32'dx;
		endcase

		if(result == 32'd0)
			isZero = 1;
		else
			isZero = 0;
	end

endmodule
