module StoreInstrSelect(
    input [1:0]address,
    input [31:0]rd,
    input [1:0]StoreSelect,
    output reg [31:0]Storewd
    );

    reg [1:0]sb_sel = address[1:0];
    reg sh_sel = address[1];

    reg [15:0]DMwd_0_15 = rd[15:0];
    reg [15:0]DMwd_16_31 = rd[31:16];

    reg [7:0]DMwd_0_7 = rd[7:0];
    reg [7:0]DMwd_8_15 = rd[15:8];
    reg [7:0]DMwd_16_23 = rd[23:16];
    reg [7:0]DMwd_24_31 = rd[31:24];

    reg [31:0]sh;
    reg [31:0]sb;

    always @(*)begin
        sh = sh_sel ? {DMwd_16_31, 16'h0000} : {16'h0000, DMwd_0_15};

        case(sb_sel)
            0:
                sb = {24'h000000, DMwd_0_7};
            1:
                sb = {16'h0000, DMwd_8_15, 8'h00};
            2:
                sb = {8'h00, DMwd_16_23, 16'h0000};
            3:
                sb = {DMwd_24_31, 24'h000000};
        endcase

        case(StoreSelect)
            0:
                Storewd = rd;
            1:
                Storewd = sh;
            2:
                Storewd = sb;
            3:
                Storewd = 0;
        endcase
    end

endmodule