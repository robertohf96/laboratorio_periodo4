module CPU(
    input clk,
    output inv); 

//register for program counter
reg [31:0]pc = 0; 

//register for my load word stall
reg [5:0]lw_reg = 0;

//pcsrc_output, this goes inside jump's mux
//wire [31:0]pcsrc_output; deprecated

//instructions
wire [31:0]instruction;

//control unit
wire jump;
wire branch;
wire MemRead;
wire MemWrite;
wire MemToReg;
wire ALUSrc;
wire RegWrite;
wire RegDst;
wire Invalid;
wire [3:0]ALUFun;
wire ExtendedOp;
wire LuiCtrl;
wire DataMemEnable;
wire RegLoadWord;
wire [1:0]LoadSelect;
wire [1:0]StoreSelct;

//registerfile
wire [31:0]write_data;
wire [4:0]write_address;
wire [31:0]read_data1;
wire [31:0]read_data2;

//alu
wire [31:0]op2;
wire [31:0]result;
wire isZero;

//ram
wire [31:0]read_data;

//LoadInIstrSelect
wire [31:0]data;

//StoreInstrSelect
wire [31:0]Storewd;

//sign_extend & 
wire [31:0]zero_extend = {16'h0000, instruction[15:0]};
wire [31:0]sign_extend = instruction[15] ? {16'hFFFF, instruction[15:0]} : {16'h0000, instruction[15:0]};

ControlUnit control_unit(instruction[31:26], instruction[5:0], jump, branch, MemRead, MemWrite, MemToReg, ALUSrc, RegWrite, 
                         RegDst, Invalid, ALUFun, ExtendedOp, LuiCtrl, DataMemEnable, RegLoadWord, LoadSelect, StoreSelct);
rom rom(clk, pc[9:2], instruction);
registerfile registerfile(clk, RegWrite, lw_reg[5], instruction[25:21], instruction[20:16], write_address, write_data, lw_reg[4:0], read_data, read_data1, read_data2);
alu alu(read_data1, op2, ALUFun, isZero, result);
StoreInstrSelect sis(result[1:0], read_data2, StoreSelct, Storewd);
ram ram(clk, MemWrite, DataMemEnable, result[7:0], Storewd, read_data);
LoadInstrSelect lis(result[1:0], read_data, LoadSelect, ExtendedOp, data);

assign write_address = RegDst ? instruction[15:11] : instruction[20:16];
assign op2 = ALUSrc ? imm_32 : read_data2;
assign write_data = LuiCtrl ? ({instruction[15:0],16'h0000}) : (MemToReg ? data : result);

//pcSrc
wire pcsrc = (branch & isZero);

//sign_extend & zero_enxtend
wire [31:0]imm_32 = ExtendedOp ? sign_extend : zero_extend;

//pc+4
wire [31:0]pc_plus_4 = pc + 4;
wire [31:0]branch_target = {imm_32<<2} + pc_plus_4;
wire [31:0]j_target = {pc_plus_4[31:28], instruction[25:0], 2'b00};

wire LwEnable = (MemRead | lw_reg[5]);

//assign my outpMemReaduts
assign inv = Invalid;

always @(posedge clk) begin
    lw_reg <= LwEnable ? ({RegLoadWord, write_address}) : lw_reg;
    pc <= jump ? j_target : (pcsrc ? branch_target : pc_plus_4 );
end

endmodule