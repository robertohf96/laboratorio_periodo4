module rom(
	input clk,
	input [7:0]address,
	output [31:0]read_data
	);
	
	initial begin
		$readmemh("code.hex", memory);
	end

	reg [31:0] memory[0:255];
	reg [31:0]data;
	always @(posedge clk)begin
		data <= memory[address];
	end

	assign read_data = data;

endmodule
	
