module ram(
    input clk,
    input MemWrite,
    input DataMemEnable,
    input [7:0]address,
    input [31:0]rd2,
    output [31:0]read_data
    );

    reg [31:0]register[0:255];
    reg [31:0]data;

    always @(posedge clk) begin
        if(DataMemEnable) begin
            if(MemWrite)
                register[address] <= rd2;

            data <= register[address];
        end
    end

    assign read_data = data;

    initial begin
    end
endmodule