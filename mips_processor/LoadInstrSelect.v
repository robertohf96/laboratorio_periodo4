module LoadInstrSelect(
    input [1:0]address,
    input [31:0]rd,
    input [1:0]lsel,
    input extend_op,
    output reg [31:0]data);

    reg [1:0]lb_sel = address[1:0];
    reg lh_sel = address[1];


    reg [15:0]DMrd_0_15 = rd[15:0];
    reg [15:0]DMrd_16_31 = rd[31:16];

    reg [15:0]lh_uex;

    reg [7:0]DMrd_0_7 = rd[7:0];
    reg [7:0]DMrd_8_15 = rd[15:8];
    reg [7:0]DMrd_16_23 = rd[23:16];
    reg [7:0]DMrd_24_31 = rd[31:24];

    reg [7:0]lb_uex;

    reg [31:0]lh;
    reg [31:0]lb;

    always @(*)begin

        lh_uex = lh_sel ? DMrd_16_31 : DMrd_0_15;

        case(lb_sel)
            0:
                lb_uex = DMrd_0_7;
            1:
                lb_uex = DMrd_8_15;
            2:
                lb_uex = DMrd_16_23;
            3:
                lb_uex = DMrd_24_31;
        endcase

        case(lsel)
            0:
                data = rd;
            1:
                data = lh;
            2:  
                data = lb;
            3:
                data = 0;
        endcase
    end


    assign lh = extend_op ? {16'hFFFF, lh_uex[15:0]} : {16'h0000, lh_uex[15:0]};
    assign lb = extend_op ? {24'hFFFFFF, lb_uex[7:0]} : {24'h000000, lb_uex[7:0]}; 
endmodule

