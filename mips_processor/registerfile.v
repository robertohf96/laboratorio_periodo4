module registerfile(
	input clk,
	input we1,
	input we2,
	input [4:0]ra1,
	input [4:0]ra2,
	input [4:0]wa1,
	input [31:0]wd1,
	input [4:0]wa2,
	input [31:0]wd2,
	output [31:0] rd1,
	output [31:0] rd2
);

reg [31:0]registeries[0:31];

always @(posedge clk) 
begin
	registeries[wa1] <= we1 ? wd1 : registeries[wa1];
	registeries[wa2] <= we2 ? wd2 : registeries[wa2];
end

assign rd1 = registeries[ra1];
assign rd2 = registeries[ra2];

initial begin
	registeries[8] = 32'hABCD1234;
	registeries[9] = 32'h1234ABCD;
	registeries[10] = 32'h5678CDAB;
end

endmodule