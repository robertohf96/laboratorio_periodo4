module ControlUnit(
    input [5:0]opcode,
    input [5:0]fn,
    output reg jump,
    output reg branch,
    output reg MemRead,
    output reg MemWrite,
    output reg MemToReg,
    output reg ALUSrc,
    output reg RegWrite,
    output reg RegDst,
    output reg invalid,
    output reg [3:0]ALUFun,
    output reg Ext_Op,
    output reg LuiCtrl,
    output reg DataMemEnable,
    output reg RegLoadWord,
    output reg [1:0]LoadSelect,
    output reg [1:0]StoreSelect
    );

    initial begin
		$readmemh("opcode_signals_verilog.hex", memory_cu, 0, 63);
        $readmemh("functions_signals_verilog.hex", memory_acu, 0, 63);
	end

    reg [21:0] memory_cu[0:63];
    reg [21:0] ControlUnit;

    reg [3:0] memory_acu[0:63];
    reg [3:0] AluControlUnit;

    reg aluOP;
    
    always @(opcode or fn) begin
        ControlUnit = memory_cu[opcode];
        AluControlUnit = memory_acu[fn];

        jump = ControlUnit[0];
        branch = ControlUnit[1];
        MemRead = ControlUnit[2];
        MemWrite = ControlUnit[3];
        MemToReg = ControlUnit[4];
        ALUSrc = ControlUnit[5];
        RegWrite = ControlUnit[6];
        RegDst = ControlUnit[7];
        aluOP = ControlUnit[8];
        Ext_Op = ControlUnit[9];
        invalid = ControlUnit[10];
        ALUFun = aluOP ? AluControlUnit[3:0] : ControlUnit[14:11];
        LuiCtrl = ControlUnit[15];
        DataMemEnable = ControlUnit[16];
        RegLoadWord = ControlUnit[17];
        LoadSelect = ControlUnit[19:18];
        StoreSelect = ControlUnit[21:20];
    end
    
endmodule