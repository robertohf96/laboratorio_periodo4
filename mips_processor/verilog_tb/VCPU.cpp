// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VCPU.h for the primary calling header

#include "VCPU.h"              // For This
#include "VCPU__Syms.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VCPU) {
    VCPU__Syms* __restrict vlSymsp = __VlSymsp = new VCPU__Syms(this, name());
    VCPU* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void VCPU::__Vconfigure(VCPU__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VCPU::~VCPU() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void VCPU::eval() {
    VCPU__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    VCPU* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate VCPU::eval\n"); );
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void VCPU::_eval_initial_loop(VCPU__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

void VCPU::_initial__TOP__1(VCPU__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VCPU::_initial__TOP__1\n"); );
    VCPU* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    //char	__VpadToAlign4[4];
    VL_SIGW(__Vtemp1,223,0,7);
    //char	__VpadToAlign36[4];
    VL_SIGW(__Vtemp2,255,0,8);
    // Body
    // INITIAL at ControlUnit.v:22
    __Vtemp1[0U] = 0x2e686578U;
    __Vtemp1[1U] = 0x696c6f67U;
    __Vtemp1[2U] = 0x5f766572U;
    __Vtemp1[3U] = 0x6e616c73U;
    __Vtemp1[4U] = 0x5f736967U;
    __Vtemp1[5U] = 0x636f6465U;
    __Vtemp1[6U] = 0x6f70U;
    VL_READMEM_W (true,22,64, 0,7, __Vtemp1, vlTOPp->CPU__DOT__control_unit__DOT__memory_cu
		  ,0U,0x3fU);
    __Vtemp2[0U] = 0x2e686578U;
    __Vtemp2[1U] = 0x696c6f67U;
    __Vtemp2[2U] = 0x5f766572U;
    __Vtemp2[3U] = 0x6e616c73U;
    __Vtemp2[4U] = 0x5f736967U;
    __Vtemp2[5U] = 0x696f6e73U;
    __Vtemp2[6U] = 0x756e6374U;
    __Vtemp2[7U] = 0x66U;
    VL_READMEM_W (true,4,64, 0,8, __Vtemp2, vlTOPp->CPU__DOT__control_unit__DOT__memory_acu
		  ,0U,0x3fU);
}

VL_INLINE_OPT void VCPU::_sequent__TOP__2(VCPU__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VCPU::_sequent__TOP__2\n"); );
    VCPU* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    VL_SIG8(__Vdly__CPU__DOT__lw_reg,5,0);
    VL_SIG8(__Vdlyvdim0__CPU__DOT__registerfile__DOT__registeries__v0,4,0);
    VL_SIG8(__Vdlyvdim0__CPU__DOT__registerfile__DOT__registeries__v1,4,0);
    VL_SIG8(__Vdlyvdim0__CPU__DOT__ram__DOT__register__v0,7,0);
    VL_SIG8(__Vdlyvset__CPU__DOT__ram__DOT__register__v0,0,0);
    //char	__VpadToAlign97[3];
    VL_SIG(__Vdlyvval__CPU__DOT__registerfile__DOT__registeries__v0,31,0);
    VL_SIG(__Vdlyvval__CPU__DOT__registerfile__DOT__registeries__v1,31,0);
    VL_SIG(__Vdlyvval__CPU__DOT__ram__DOT__register__v0,31,0);
    // Body
    __Vdly__CPU__DOT__lw_reg = vlTOPp->CPU__DOT__lw_reg;
    __Vdlyvset__CPU__DOT__ram__DOT__register__v0 = 0U;
    // ALWAYS at CPU.v:89
    __Vdly__CPU__DOT__lw_reg = ((IData)(vlTOPp->CPU__DOT__LwEnable)
				 ? (((IData)(vlTOPp->CPU__DOT__RegLoadWord) 
				     << 5U) | (IData)(vlTOPp->CPU__DOT__write_address))
				 : (IData)(vlTOPp->CPU__DOT__lw_reg));
    // ALWAYS at registerfile.v:17
    __Vdlyvval__CPU__DOT__registerfile__DOT__registeries__v0 
	= ((IData)(vlTOPp->CPU__DOT__RegWrite) ? ((IData)(vlTOPp->CPU__DOT__LuiCtrl)
						   ? 
						  (0xffff0000U 
						   & (vlTOPp->CPU__DOT__rom__DOT__data 
						      << 0x10U))
						   : 
						  ((IData)(vlTOPp->CPU__DOT__MemToReg)
						    ? vlTOPp->CPU__DOT__data
						    : vlTOPp->CPU__DOT__result))
	    : vlTOPp->CPU__DOT__registerfile__DOT__registeries
	   [vlTOPp->CPU__DOT__write_address]);
    __Vdlyvdim0__CPU__DOT__registerfile__DOT__registeries__v0 
	= vlTOPp->CPU__DOT__write_address;
    __Vdlyvval__CPU__DOT__registerfile__DOT__registeries__v1 
	= ((0x20U & (IData)(vlTOPp->CPU__DOT__lw_reg))
	    ? vlTOPp->CPU__DOT__ram__DOT__data : vlTOPp->CPU__DOT__registerfile__DOT__registeries
	   [(0x1fU & (IData)(vlTOPp->CPU__DOT__lw_reg))]);
    __Vdlyvdim0__CPU__DOT__registerfile__DOT__registeries__v1 
	= (0x1fU & (IData)(vlTOPp->CPU__DOT__lw_reg));
    vlTOPp->CPU__DOT__lw_reg = __Vdly__CPU__DOT__lw_reg;
    // ALWAYSPOST at registerfile.v:19
    vlTOPp->CPU__DOT__registerfile__DOT__registeries[__Vdlyvdim0__CPU__DOT__registerfile__DOT__registeries__v0] 
	= __Vdlyvval__CPU__DOT__registerfile__DOT__registeries__v0;
    vlTOPp->CPU__DOT__registerfile__DOT__registeries[__Vdlyvdim0__CPU__DOT__registerfile__DOT__registeries__v1] 
	= __Vdlyvval__CPU__DOT__registerfile__DOT__registeries__v1;
    // ALWAYS at ram.v:13
    if (vlTOPp->CPU__DOT__DataMemEnable) {
	vlTOPp->CPU__DOT__ram__DOT__data = vlTOPp->CPU__DOT__ram__DOT__register
	    [(0xffU & vlTOPp->CPU__DOT__result)];
	if (vlTOPp->CPU__DOT__MemWrite) {
	    __Vdlyvval__CPU__DOT__ram__DOT__register__v0 
		= vlTOPp->CPU__DOT__Storewd;
	    __Vdlyvset__CPU__DOT__ram__DOT__register__v0 = 1U;
	    __Vdlyvdim0__CPU__DOT__ram__DOT__register__v0 
		= (0xffU & vlTOPp->CPU__DOT__result);
	}
    }
    // ALWAYS at rom.v:13
    vlTOPp->CPU__DOT__rom__DOT__data = vlTOPp->CPU__DOT__rom__DOT__memory
	[(0xffU & (vlTOPp->CPU__DOT__pc >> 2U))];
    // ALWAYSPOST at ram.v:16
    if (__Vdlyvset__CPU__DOT__ram__DOT__register__v0) {
	vlTOPp->CPU__DOT__ram__DOT__register[__Vdlyvdim0__CPU__DOT__ram__DOT__register__v0] 
	    = __Vdlyvval__CPU__DOT__ram__DOT__register__v0;
    }
    // ALWAYS at CPU.v:90
    vlTOPp->CPU__DOT__pc = ((IData)(vlTOPp->CPU__DOT__jump)
			     ? vlTOPp->CPU__DOT__j_target
			     : (((IData)(vlTOPp->CPU__DOT__branch) 
				 & (IData)(vlTOPp->CPU__DOT__isZero))
				 ? vlTOPp->CPU__DOT__branch_target
				 : vlTOPp->CPU__DOT__pc_plus_4));
    // ALWAYS at ControlUnit.v:35
    vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
	= vlTOPp->CPU__DOT__control_unit__DOT__memory_cu
	[(0x3fU & (vlTOPp->CPU__DOT__rom__DOT__data 
		   >> 0x1aU))];
    vlTOPp->CPU__DOT__control_unit__DOT__AluControlUnit 
	= vlTOPp->CPU__DOT__control_unit__DOT__memory_acu
	[(0x3fU & vlTOPp->CPU__DOT__rom__DOT__data)];
    vlTOPp->CPU__DOT__jump = (1U & vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit);
    vlTOPp->CPU__DOT__branch = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
				      >> 1U));
    vlTOPp->CPU__DOT__MemRead = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
				       >> 2U));
    vlTOPp->CPU__DOT__MemWrite = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					>> 3U));
    vlTOPp->CPU__DOT__MemToReg = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					>> 4U));
    vlTOPp->CPU__DOT__ALUSrc = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
				      >> 5U));
    vlTOPp->CPU__DOT__RegWrite = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					>> 6U));
    vlTOPp->CPU__DOT__RegDst = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
				      >> 7U));
    vlTOPp->CPU__DOT__control_unit__DOT__aluOP = (1U 
						  & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
						     >> 8U));
    vlTOPp->CPU__DOT__ExtendedOp = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					  >> 9U));
    vlTOPp->CPU__DOT__Invalid = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
				       >> 0xaU));
    vlTOPp->CPU__DOT__ALUFun = (0xfU & ((IData)(vlTOPp->CPU__DOT__control_unit__DOT__aluOP)
					 ? (IData)(vlTOPp->CPU__DOT__control_unit__DOT__AluControlUnit)
					 : (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					    >> 0xbU)));
    vlTOPp->CPU__DOT__LuiCtrl = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
				       >> 0xfU));
    vlTOPp->CPU__DOT__DataMemEnable = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					     >> 0x10U));
    vlTOPp->CPU__DOT__RegLoadWord = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					   >> 0x11U));
    vlTOPp->CPU__DOT__LoadSelect = (3U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					  >> 0x12U));
    vlTOPp->CPU__DOT__StoreSelct = (3U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					  >> 0x14U));
}

void VCPU::_initial__TOP__3(VCPU__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VCPU::_initial__TOP__3\n"); );
    VCPU* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // INITIAL at CPU.v:9
    vlTOPp->CPU__DOT__lw_reg = 0U;
    // INITIAL at registerfile.v:26
    vlTOPp->CPU__DOT__registerfile__DOT__registeries[8U] = 0xabcd1234U;
    vlTOPp->CPU__DOT__registerfile__DOT__registeries[9U] = 0x1234abcdU;
    vlTOPp->CPU__DOT__registerfile__DOT__registeries[0xaU] = 0x5678cdabU;
    // INITIAL at rom.v:7
    VL_READMEM_Q (true,32,256, 0,2, VL_ULL(0x636f64652e686578)
		  , vlTOPp->CPU__DOT__rom__DOT__memory
		  ,0,~0);
    // INITIAL at CPU.v:6
    vlTOPp->CPU__DOT__pc = 0U;
}

void VCPU::_settle__TOP__4(VCPU__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VCPU::_settle__TOP__4\n"); );
    VCPU* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at ControlUnit.v:35
    vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
	= vlTOPp->CPU__DOT__control_unit__DOT__memory_cu
	[(0x3fU & (vlTOPp->CPU__DOT__rom__DOT__data 
		   >> 0x1aU))];
    vlTOPp->CPU__DOT__control_unit__DOT__AluControlUnit 
	= vlTOPp->CPU__DOT__control_unit__DOT__memory_acu
	[(0x3fU & vlTOPp->CPU__DOT__rom__DOT__data)];
    vlTOPp->CPU__DOT__jump = (1U & vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit);
    vlTOPp->CPU__DOT__branch = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
				      >> 1U));
    vlTOPp->CPU__DOT__MemRead = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
				       >> 2U));
    vlTOPp->CPU__DOT__MemWrite = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					>> 3U));
    vlTOPp->CPU__DOT__MemToReg = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					>> 4U));
    vlTOPp->CPU__DOT__ALUSrc = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
				      >> 5U));
    vlTOPp->CPU__DOT__RegWrite = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					>> 6U));
    vlTOPp->CPU__DOT__RegDst = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
				      >> 7U));
    vlTOPp->CPU__DOT__control_unit__DOT__aluOP = (1U 
						  & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
						     >> 8U));
    vlTOPp->CPU__DOT__ExtendedOp = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					  >> 9U));
    vlTOPp->CPU__DOT__Invalid = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
				       >> 0xaU));
    vlTOPp->CPU__DOT__ALUFun = (0xfU & ((IData)(vlTOPp->CPU__DOT__control_unit__DOT__aluOP)
					 ? (IData)(vlTOPp->CPU__DOT__control_unit__DOT__AluControlUnit)
					 : (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					    >> 0xbU)));
    vlTOPp->CPU__DOT__LuiCtrl = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
				       >> 0xfU));
    vlTOPp->CPU__DOT__DataMemEnable = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					     >> 0x10U));
    vlTOPp->CPU__DOT__RegLoadWord = (1U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					   >> 0x11U));
    vlTOPp->CPU__DOT__LoadSelect = (3U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					  >> 0x12U));
    vlTOPp->CPU__DOT__StoreSelct = (3U & (vlTOPp->CPU__DOT__control_unit__DOT__ControlUnit 
					  >> 0x14U));
    vlTOPp->CPU__DOT__read_data1 = vlTOPp->CPU__DOT__registerfile__DOT__registeries
	[(0x1fU & (vlTOPp->CPU__DOT__rom__DOT__data 
		   >> 0x15U))];
    vlTOPp->CPU__DOT__read_data2 = vlTOPp->CPU__DOT__registerfile__DOT__registeries
	[(0x1fU & (vlTOPp->CPU__DOT__rom__DOT__data 
		   >> 0x10U))];
    vlTOPp->CPU__DOT__pc_plus_4 = ((IData)(4U) + vlTOPp->CPU__DOT__pc);
    vlTOPp->CPU__DOT__j_target = ((0xf0000000U & ((IData)(4U) 
						  + vlTOPp->CPU__DOT__pc)) 
				  | (0xffffffcU & (vlTOPp->CPU__DOT__rom__DOT__data 
						   << 2U)));
    vlTOPp->inv = vlTOPp->CPU__DOT__Invalid;
    vlTOPp->CPU__DOT__LwEnable = (1U & ((IData)(vlTOPp->CPU__DOT__MemRead) 
					| ((IData)(vlTOPp->CPU__DOT__lw_reg) 
					   >> 5U)));
    vlTOPp->CPU__DOT__write_address = (0x1fU & ((IData)(vlTOPp->CPU__DOT__RegDst)
						 ? 
						(vlTOPp->CPU__DOT__rom__DOT__data 
						 >> 0xbU)
						 : 
						(vlTOPp->CPU__DOT__rom__DOT__data 
						 >> 0x10U)));
    vlTOPp->CPU__DOT__imm_32 = ((IData)(vlTOPp->CPU__DOT__ExtendedOp)
				 ? ((0x8000U & vlTOPp->CPU__DOT__rom__DOT__data)
				     ? (0xffff0000U 
					| (0xffffU 
					   & vlTOPp->CPU__DOT__rom__DOT__data))
				     : (0xffffU & vlTOPp->CPU__DOT__rom__DOT__data))
				 : (0xffffU & vlTOPp->CPU__DOT__rom__DOT__data));
}

VL_INLINE_OPT void VCPU::_sequent__TOP__5(VCPU__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VCPU::_sequent__TOP__5\n"); );
    VCPU* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->CPU__DOT__read_data1 = vlTOPp->CPU__DOT__registerfile__DOT__registeries
	[(0x1fU & (vlTOPp->CPU__DOT__rom__DOT__data 
		   >> 0x15U))];
    vlTOPp->CPU__DOT__read_data2 = vlTOPp->CPU__DOT__registerfile__DOT__registeries
	[(0x1fU & (vlTOPp->CPU__DOT__rom__DOT__data 
		   >> 0x10U))];
    vlTOPp->CPU__DOT__pc_plus_4 = ((IData)(4U) + vlTOPp->CPU__DOT__pc);
    vlTOPp->CPU__DOT__j_target = ((0xf0000000U & ((IData)(4U) 
						  + vlTOPp->CPU__DOT__pc)) 
				  | (0xffffffcU & (vlTOPp->CPU__DOT__rom__DOT__data 
						   << 2U)));
    vlTOPp->inv = vlTOPp->CPU__DOT__Invalid;
    vlTOPp->CPU__DOT__LwEnable = (1U & ((IData)(vlTOPp->CPU__DOT__MemRead) 
					| ((IData)(vlTOPp->CPU__DOT__lw_reg) 
					   >> 5U)));
    vlTOPp->CPU__DOT__write_address = (0x1fU & ((IData)(vlTOPp->CPU__DOT__RegDst)
						 ? 
						(vlTOPp->CPU__DOT__rom__DOT__data 
						 >> 0xbU)
						 : 
						(vlTOPp->CPU__DOT__rom__DOT__data 
						 >> 0x10U)));
    vlTOPp->CPU__DOT__imm_32 = ((IData)(vlTOPp->CPU__DOT__ExtendedOp)
				 ? ((0x8000U & vlTOPp->CPU__DOT__rom__DOT__data)
				     ? (0xffff0000U 
					| (0xffffU 
					   & vlTOPp->CPU__DOT__rom__DOT__data))
				     : (0xffffU & vlTOPp->CPU__DOT__rom__DOT__data))
				 : (0xffffU & vlTOPp->CPU__DOT__rom__DOT__data));
    vlTOPp->CPU__DOT__branch_target = ((IData)(4U) 
				       + ((vlTOPp->CPU__DOT__imm_32 
					   << 2U) + vlTOPp->CPU__DOT__pc));
    vlTOPp->CPU__DOT__op2 = ((IData)(vlTOPp->CPU__DOT__ALUSrc)
			      ? vlTOPp->CPU__DOT__imm_32
			      : vlTOPp->CPU__DOT__read_data2);
}

void VCPU::_settle__TOP__6(VCPU__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VCPU::_settle__TOP__6\n"); );
    VCPU* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->CPU__DOT__branch_target = ((IData)(4U) 
				       + ((vlTOPp->CPU__DOT__imm_32 
					   << 2U) + vlTOPp->CPU__DOT__pc));
    vlTOPp->CPU__DOT__op2 = ((IData)(vlTOPp->CPU__DOT__ALUSrc)
			      ? vlTOPp->CPU__DOT__imm_32
			      : vlTOPp->CPU__DOT__read_data2);
    // ALWAYS at alu.v:11
    vlTOPp->CPU__DOT__result = ((8U & (IData)(vlTOPp->CPU__DOT__ALUFun))
				 ? 0U : ((4U & (IData)(vlTOPp->CPU__DOT__ALUFun))
					  ? ((2U & (IData)(vlTOPp->CPU__DOT__ALUFun))
					      ? ((1U 
						  & (IData)(vlTOPp->CPU__DOT__ALUFun))
						  ? 
						 ((0x1fU 
						   >= vlTOPp->CPU__DOT__op2)
						   ? 
						  (vlTOPp->CPU__DOT__read_data1 
						   << vlTOPp->CPU__DOT__op2)
						   : 0U)
						  : 0U)
					      : ((1U 
						  & (IData)(vlTOPp->CPU__DOT__ALUFun))
						  ? 0U
						  : 
						 VL_LTS_III(32,32,32, vlTOPp->CPU__DOT__read_data1, vlTOPp->CPU__DOT__op2)))
					  : ((2U & (IData)(vlTOPp->CPU__DOT__ALUFun))
					      ? ((1U 
						  & (IData)(vlTOPp->CPU__DOT__ALUFun))
						  ? 
						 (vlTOPp->CPU__DOT__read_data1 
						  | vlTOPp->CPU__DOT__op2)
						  : 
						 (vlTOPp->CPU__DOT__read_data1 
						  & vlTOPp->CPU__DOT__op2))
					      : ((1U 
						  & (IData)(vlTOPp->CPU__DOT__ALUFun))
						  ? 
						 (vlTOPp->CPU__DOT__read_data1 
						  - vlTOPp->CPU__DOT__op2)
						  : 
						 (vlTOPp->CPU__DOT__read_data1 
						  + vlTOPp->CPU__DOT__op2)))));
    vlTOPp->CPU__DOT__isZero = (0U == vlTOPp->CPU__DOT__result);
}

VL_INLINE_OPT void VCPU::_sequent__TOP__7(VCPU__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VCPU::_sequent__TOP__7\n"); );
    VCPU* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at alu.v:11
    vlTOPp->CPU__DOT__result = ((8U & (IData)(vlTOPp->CPU__DOT__ALUFun))
				 ? 0U : ((4U & (IData)(vlTOPp->CPU__DOT__ALUFun))
					  ? ((2U & (IData)(vlTOPp->CPU__DOT__ALUFun))
					      ? ((1U 
						  & (IData)(vlTOPp->CPU__DOT__ALUFun))
						  ? 
						 ((0x1fU 
						   >= vlTOPp->CPU__DOT__op2)
						   ? 
						  (vlTOPp->CPU__DOT__read_data1 
						   << vlTOPp->CPU__DOT__op2)
						   : 0U)
						  : 0U)
					      : ((1U 
						  & (IData)(vlTOPp->CPU__DOT__ALUFun))
						  ? 0U
						  : 
						 VL_LTS_III(32,32,32, vlTOPp->CPU__DOT__read_data1, vlTOPp->CPU__DOT__op2)))
					  : ((2U & (IData)(vlTOPp->CPU__DOT__ALUFun))
					      ? ((1U 
						  & (IData)(vlTOPp->CPU__DOT__ALUFun))
						  ? 
						 (vlTOPp->CPU__DOT__read_data1 
						  | vlTOPp->CPU__DOT__op2)
						  : 
						 (vlTOPp->CPU__DOT__read_data1 
						  & vlTOPp->CPU__DOT__op2))
					      : ((1U 
						  & (IData)(vlTOPp->CPU__DOT__ALUFun))
						  ? 
						 (vlTOPp->CPU__DOT__read_data1 
						  - vlTOPp->CPU__DOT__op2)
						  : 
						 (vlTOPp->CPU__DOT__read_data1 
						  + vlTOPp->CPU__DOT__op2)))));
    vlTOPp->CPU__DOT__isZero = (0U == vlTOPp->CPU__DOT__result);
    // ALWAYS at StoreInstrSelect.v:22
    vlTOPp->CPU__DOT__sis__DOT__sh = ((2U & vlTOPp->CPU__DOT__result)
				       ? (0xffff0000U 
					  & vlTOPp->CPU__DOT__read_data2)
				       : (0xffffU & vlTOPp->CPU__DOT__read_data2));
    if ((0U == (3U & vlTOPp->CPU__DOT__result))) {
	vlTOPp->CPU__DOT__sis__DOT__sb = (0xffU & vlTOPp->CPU__DOT__read_data2);
    } else {
	if ((1U == (3U & vlTOPp->CPU__DOT__result))) {
	    vlTOPp->CPU__DOT__sis__DOT__sb = (0xff00U 
					      & vlTOPp->CPU__DOT__read_data2);
	} else {
	    if ((2U == (3U & vlTOPp->CPU__DOT__result))) {
		vlTOPp->CPU__DOT__sis__DOT__sb = (0xff0000U 
						  & vlTOPp->CPU__DOT__read_data2);
	    } else {
		if ((3U == (3U & vlTOPp->CPU__DOT__result))) {
		    vlTOPp->CPU__DOT__sis__DOT__sb 
			= (0xff000000U & vlTOPp->CPU__DOT__read_data2);
		}
	    }
	}
    }
    if ((0U == (IData)(vlTOPp->CPU__DOT__StoreSelct))) {
	vlTOPp->CPU__DOT__Storewd = vlTOPp->CPU__DOT__read_data2;
    } else {
	if ((1U == (IData)(vlTOPp->CPU__DOT__StoreSelct))) {
	    vlTOPp->CPU__DOT__Storewd = vlTOPp->CPU__DOT__sis__DOT__sh;
	} else {
	    if ((2U == (IData)(vlTOPp->CPU__DOT__StoreSelct))) {
		vlTOPp->CPU__DOT__Storewd = vlTOPp->CPU__DOT__sis__DOT__sb;
	    } else {
		if ((3U == (IData)(vlTOPp->CPU__DOT__StoreSelct))) {
		    vlTOPp->CPU__DOT__Storewd = 0U;
		}
	    }
	}
    }
    // ALWAYS at LoadInstrSelect.v:29
    vlTOPp->CPU__DOT__lis__DOT__lh_uex = (0xffffU & 
					  ((2U & vlTOPp->CPU__DOT__result)
					    ? (vlTOPp->CPU__DOT__ram__DOT__data 
					       >> 0x10U)
					    : vlTOPp->CPU__DOT__ram__DOT__data));
    // ALWAYS at LoadInstrSelect.v:32
    if ((0U == (3U & vlTOPp->CPU__DOT__result))) {
	vlTOPp->CPU__DOT__lis__DOT__lb_uex = (0xffU 
					      & vlTOPp->CPU__DOT__ram__DOT__data);
    } else {
	if ((1U == (3U & vlTOPp->CPU__DOT__result))) {
	    vlTOPp->CPU__DOT__lis__DOT__lb_uex = (0xffU 
						  & (vlTOPp->CPU__DOT__ram__DOT__data 
						     >> 8U));
	} else {
	    if ((2U == (3U & vlTOPp->CPU__DOT__result))) {
		vlTOPp->CPU__DOT__lis__DOT__lb_uex 
		    = (0xffU & (vlTOPp->CPU__DOT__ram__DOT__data 
				>> 0x10U));
	    } else {
		if ((3U == (3U & vlTOPp->CPU__DOT__result))) {
		    vlTOPp->CPU__DOT__lis__DOT__lb_uex 
			= (0xffU & (vlTOPp->CPU__DOT__ram__DOT__data 
				    >> 0x18U));
		}
	    }
	}
    }
}

void VCPU::_settle__TOP__8(VCPU__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VCPU::_settle__TOP__8\n"); );
    VCPU* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at StoreInstrSelect.v:22
    vlTOPp->CPU__DOT__sis__DOT__sh = ((2U & vlTOPp->CPU__DOT__result)
				       ? (0xffff0000U 
					  & vlTOPp->CPU__DOT__read_data2)
				       : (0xffffU & vlTOPp->CPU__DOT__read_data2));
    if ((0U == (3U & vlTOPp->CPU__DOT__result))) {
	vlTOPp->CPU__DOT__sis__DOT__sb = (0xffU & vlTOPp->CPU__DOT__read_data2);
    } else {
	if ((1U == (3U & vlTOPp->CPU__DOT__result))) {
	    vlTOPp->CPU__DOT__sis__DOT__sb = (0xff00U 
					      & vlTOPp->CPU__DOT__read_data2);
	} else {
	    if ((2U == (3U & vlTOPp->CPU__DOT__result))) {
		vlTOPp->CPU__DOT__sis__DOT__sb = (0xff0000U 
						  & vlTOPp->CPU__DOT__read_data2);
	    } else {
		if ((3U == (3U & vlTOPp->CPU__DOT__result))) {
		    vlTOPp->CPU__DOT__sis__DOT__sb 
			= (0xff000000U & vlTOPp->CPU__DOT__read_data2);
		}
	    }
	}
    }
    if ((0U == (IData)(vlTOPp->CPU__DOT__StoreSelct))) {
	vlTOPp->CPU__DOT__Storewd = vlTOPp->CPU__DOT__read_data2;
    } else {
	if ((1U == (IData)(vlTOPp->CPU__DOT__StoreSelct))) {
	    vlTOPp->CPU__DOT__Storewd = vlTOPp->CPU__DOT__sis__DOT__sh;
	} else {
	    if ((2U == (IData)(vlTOPp->CPU__DOT__StoreSelct))) {
		vlTOPp->CPU__DOT__Storewd = vlTOPp->CPU__DOT__sis__DOT__sb;
	    } else {
		if ((3U == (IData)(vlTOPp->CPU__DOT__StoreSelct))) {
		    vlTOPp->CPU__DOT__Storewd = 0U;
		}
	    }
	}
    }
    // ALWAYS at LoadInstrSelect.v:29
    vlTOPp->CPU__DOT__lis__DOT__lh_uex = (0xffffU & 
					  ((2U & vlTOPp->CPU__DOT__result)
					    ? (vlTOPp->CPU__DOT__ram__DOT__data 
					       >> 0x10U)
					    : vlTOPp->CPU__DOT__ram__DOT__data));
    // ALWAYS at LoadInstrSelect.v:32
    if ((0U == (3U & vlTOPp->CPU__DOT__result))) {
	vlTOPp->CPU__DOT__lis__DOT__lb_uex = (0xffU 
					      & vlTOPp->CPU__DOT__ram__DOT__data);
    } else {
	if ((1U == (3U & vlTOPp->CPU__DOT__result))) {
	    vlTOPp->CPU__DOT__lis__DOT__lb_uex = (0xffU 
						  & (vlTOPp->CPU__DOT__ram__DOT__data 
						     >> 8U));
	} else {
	    if ((2U == (3U & vlTOPp->CPU__DOT__result))) {
		vlTOPp->CPU__DOT__lis__DOT__lb_uex 
		    = (0xffU & (vlTOPp->CPU__DOT__ram__DOT__data 
				>> 0x10U));
	    } else {
		if ((3U == (3U & vlTOPp->CPU__DOT__result))) {
		    vlTOPp->CPU__DOT__lis__DOT__lb_uex 
			= (0xffU & (vlTOPp->CPU__DOT__ram__DOT__data 
				    >> 0x18U));
		}
	    }
	}
    }
    // ALWAYS at LoadInstrSelect.v:43
    if ((0U == (IData)(vlTOPp->CPU__DOT__LoadSelect))) {
	vlTOPp->CPU__DOT__data = vlTOPp->CPU__DOT__ram__DOT__data;
    } else {
	if ((1U == (IData)(vlTOPp->CPU__DOT__LoadSelect))) {
	    vlTOPp->CPU__DOT__data = ((IData)(vlTOPp->CPU__DOT__ExtendedOp)
				       ? (0xffff0000U 
					  | (IData)(vlTOPp->CPU__DOT__lis__DOT__lh_uex))
				       : (IData)(vlTOPp->CPU__DOT__lis__DOT__lh_uex));
	} else {
	    if ((2U == (IData)(vlTOPp->CPU__DOT__LoadSelect))) {
		vlTOPp->CPU__DOT__data = ((IData)(vlTOPp->CPU__DOT__ExtendedOp)
					   ? (0xffffff00U 
					      | (IData)(vlTOPp->CPU__DOT__lis__DOT__lb_uex))
					   : (IData)(vlTOPp->CPU__DOT__lis__DOT__lb_uex));
	    } else {
		if ((3U == (IData)(vlTOPp->CPU__DOT__LoadSelect))) {
		    vlTOPp->CPU__DOT__data = 0U;
		}
	    }
	}
    }
}

VL_INLINE_OPT void VCPU::_sequent__TOP__9(VCPU__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VCPU::_sequent__TOP__9\n"); );
    VCPU* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at LoadInstrSelect.v:43
    if ((0U == (IData)(vlTOPp->CPU__DOT__LoadSelect))) {
	vlTOPp->CPU__DOT__data = vlTOPp->CPU__DOT__ram__DOT__data;
    } else {
	if ((1U == (IData)(vlTOPp->CPU__DOT__LoadSelect))) {
	    vlTOPp->CPU__DOT__data = ((IData)(vlTOPp->CPU__DOT__ExtendedOp)
				       ? (0xffff0000U 
					  | (IData)(vlTOPp->CPU__DOT__lis__DOT__lh_uex))
				       : (IData)(vlTOPp->CPU__DOT__lis__DOT__lh_uex));
	} else {
	    if ((2U == (IData)(vlTOPp->CPU__DOT__LoadSelect))) {
		vlTOPp->CPU__DOT__data = ((IData)(vlTOPp->CPU__DOT__ExtendedOp)
					   ? (0xffffff00U 
					      | (IData)(vlTOPp->CPU__DOT__lis__DOT__lb_uex))
					   : (IData)(vlTOPp->CPU__DOT__lis__DOT__lb_uex));
	    } else {
		if ((3U == (IData)(vlTOPp->CPU__DOT__LoadSelect))) {
		    vlTOPp->CPU__DOT__data = 0U;
		}
	    }
	}
    }
}

void VCPU::_eval(VCPU__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VCPU::_eval\n"); );
    VCPU* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (((IData)(vlTOPp->clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__clk)))) {
	vlTOPp->_sequent__TOP__2(vlSymsp);
	vlTOPp->_sequent__TOP__5(vlSymsp);
	vlTOPp->_sequent__TOP__7(vlSymsp);
	vlTOPp->_sequent__TOP__9(vlSymsp);
    }
    // Final
    vlTOPp->__Vclklast__TOP__clk = vlTOPp->clk;
}

void VCPU::_eval_initial(VCPU__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VCPU::_eval_initial\n"); );
    VCPU* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_initial__TOP__1(vlSymsp);
    vlTOPp->_initial__TOP__3(vlSymsp);
}

void VCPU::final() {
    VL_DEBUG_IF(VL_PRINTF("    VCPU::final\n"); );
    // Variables
    VCPU__Syms* __restrict vlSymsp = this->__VlSymsp;
    VCPU* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VCPU::_eval_settle(VCPU__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VCPU::_eval_settle\n"); );
    VCPU* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_settle__TOP__4(vlSymsp);
    vlTOPp->_settle__TOP__6(vlSymsp);
    vlTOPp->_settle__TOP__8(vlSymsp);
}

VL_INLINE_OPT QData VCPU::_change_request(VCPU__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VCPU::_change_request\n"); );
    VCPU* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    return __req;
}

void VCPU::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("    VCPU::_ctor_var_reset\n"); );
    // Body
    clk = VL_RAND_RESET_I(1);
    inv = VL_RAND_RESET_I(1);
    CPU__DOT__pc = VL_RAND_RESET_I(32);
    CPU__DOT__lw_reg = VL_RAND_RESET_I(6);
    CPU__DOT__jump = VL_RAND_RESET_I(1);
    CPU__DOT__branch = VL_RAND_RESET_I(1);
    CPU__DOT__MemRead = VL_RAND_RESET_I(1);
    CPU__DOT__MemWrite = VL_RAND_RESET_I(1);
    CPU__DOT__MemToReg = VL_RAND_RESET_I(1);
    CPU__DOT__ALUSrc = VL_RAND_RESET_I(1);
    CPU__DOT__RegWrite = VL_RAND_RESET_I(1);
    CPU__DOT__RegDst = VL_RAND_RESET_I(1);
    CPU__DOT__Invalid = VL_RAND_RESET_I(1);
    CPU__DOT__ALUFun = VL_RAND_RESET_I(4);
    CPU__DOT__ExtendedOp = VL_RAND_RESET_I(1);
    CPU__DOT__LuiCtrl = VL_RAND_RESET_I(1);
    CPU__DOT__DataMemEnable = VL_RAND_RESET_I(1);
    CPU__DOT__RegLoadWord = VL_RAND_RESET_I(1);
    CPU__DOT__LoadSelect = VL_RAND_RESET_I(2);
    CPU__DOT__StoreSelct = VL_RAND_RESET_I(2);
    CPU__DOT__write_address = VL_RAND_RESET_I(5);
    CPU__DOT__read_data1 = VL_RAND_RESET_I(32);
    CPU__DOT__read_data2 = VL_RAND_RESET_I(32);
    CPU__DOT__op2 = VL_RAND_RESET_I(32);
    CPU__DOT__result = VL_RAND_RESET_I(32);
    CPU__DOT__isZero = VL_RAND_RESET_I(1);
    CPU__DOT__data = VL_RAND_RESET_I(32);
    CPU__DOT__Storewd = VL_RAND_RESET_I(32);
    CPU__DOT__imm_32 = VL_RAND_RESET_I(32);
    CPU__DOT__pc_plus_4 = VL_RAND_RESET_I(32);
    CPU__DOT__branch_target = VL_RAND_RESET_I(32);
    CPU__DOT__j_target = VL_RAND_RESET_I(32);
    CPU__DOT__LwEnable = VL_RAND_RESET_I(1);
    { int __Vi0=0; for (; __Vi0<64; ++__Vi0) {
	    CPU__DOT__control_unit__DOT__memory_cu[__Vi0] = VL_RAND_RESET_I(22);
    }}
    CPU__DOT__control_unit__DOT__ControlUnit = VL_RAND_RESET_I(22);
    { int __Vi0=0; for (; __Vi0<64; ++__Vi0) {
	    CPU__DOT__control_unit__DOT__memory_acu[__Vi0] = VL_RAND_RESET_I(4);
    }}
    CPU__DOT__control_unit__DOT__AluControlUnit = VL_RAND_RESET_I(4);
    CPU__DOT__control_unit__DOT__aluOP = VL_RAND_RESET_I(1);
    { int __Vi0=0; for (; __Vi0<256; ++__Vi0) {
	    CPU__DOT__rom__DOT__memory[__Vi0] = VL_RAND_RESET_I(32);
    }}
    CPU__DOT__rom__DOT__data = VL_RAND_RESET_I(32);
    { int __Vi0=0; for (; __Vi0<32; ++__Vi0) {
	    CPU__DOT__registerfile__DOT__registeries[__Vi0] = VL_RAND_RESET_I(32);
    }}
    CPU__DOT__sis__DOT__sh = VL_RAND_RESET_I(32);
    CPU__DOT__sis__DOT__sb = VL_RAND_RESET_I(32);
    { int __Vi0=0; for (; __Vi0<256; ++__Vi0) {
	    CPU__DOT__ram__DOT__register[__Vi0] = VL_RAND_RESET_I(32);
    }}
    CPU__DOT__ram__DOT__data = VL_RAND_RESET_I(32);
    CPU__DOT__lis__DOT__lh_uex = VL_RAND_RESET_I(16);
    CPU__DOT__lis__DOT__lb_uex = VL_RAND_RESET_I(8);
    __Vclklast__TOP__clk = VL_RAND_RESET_I(1);
}

void VCPU::_configure_coverage(VCPU__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("    VCPU::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
