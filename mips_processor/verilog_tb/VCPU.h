// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Primary design header
//
// This header should be included by all source files instantiating the design.
// The class here is then constructed to instantiate the design.
// See the Verilator manual for examples.

#ifndef _VCPU_H_
#define _VCPU_H_

#include "verilated.h"
class VCPU__Syms;

//----------

VL_MODULE(VCPU) {
  public:
    // CELLS
    // Public to allow access to /*verilator_public*/ items;
    // otherwise the application code can consider these internals.
    
    // PORTS
    // The application code writes and reads these signals to
    // propagate new values into/out from the Verilated model.
    VL_IN8(clk,0,0);
    VL_OUT8(inv,0,0);
    //char	__VpadToAlign2[2];
    
    // LOCAL SIGNALS
    // Internals; generally not touched by application code
    VL_SIG8(CPU__DOT__lw_reg,5,0);
    VL_SIG8(CPU__DOT__jump,0,0);
    VL_SIG8(CPU__DOT__branch,0,0);
    VL_SIG8(CPU__DOT__MemRead,0,0);
    VL_SIG8(CPU__DOT__MemWrite,0,0);
    VL_SIG8(CPU__DOT__MemToReg,0,0);
    VL_SIG8(CPU__DOT__ALUSrc,0,0);
    VL_SIG8(CPU__DOT__RegWrite,0,0);
    VL_SIG8(CPU__DOT__RegDst,0,0);
    VL_SIG8(CPU__DOT__Invalid,0,0);
    VL_SIG8(CPU__DOT__ALUFun,3,0);
    VL_SIG8(CPU__DOT__ExtendedOp,0,0);
    VL_SIG8(CPU__DOT__LuiCtrl,0,0);
    VL_SIG8(CPU__DOT__DataMemEnable,0,0);
    VL_SIG8(CPU__DOT__RegLoadWord,0,0);
    VL_SIG8(CPU__DOT__LoadSelect,1,0);
    VL_SIG8(CPU__DOT__StoreSelct,1,0);
    VL_SIG8(CPU__DOT__write_address,4,0);
    VL_SIG8(CPU__DOT__isZero,0,0);
    VL_SIG8(CPU__DOT__LwEnable,0,0);
    VL_SIG8(CPU__DOT__control_unit__DOT__AluControlUnit,3,0);
    VL_SIG8(CPU__DOT__control_unit__DOT__aluOP,0,0);
    VL_SIG8(CPU__DOT__lis__DOT__lb_uex,7,0);
    //char	__VpadToAlign31[1];
    VL_SIG16(CPU__DOT__lis__DOT__lh_uex,15,0);
    //char	__VpadToAlign34[2];
    VL_SIG(CPU__DOT__pc,31,0);
    VL_SIG(CPU__DOT__read_data1,31,0);
    VL_SIG(CPU__DOT__read_data2,31,0);
    VL_SIG(CPU__DOT__op2,31,0);
    VL_SIG(CPU__DOT__result,31,0);
    VL_SIG(CPU__DOT__data,31,0);
    VL_SIG(CPU__DOT__Storewd,31,0);
    VL_SIG(CPU__DOT__imm_32,31,0);
    VL_SIG(CPU__DOT__pc_plus_4,31,0);
    VL_SIG(CPU__DOT__branch_target,31,0);
    VL_SIG(CPU__DOT__j_target,31,0);
    VL_SIG(CPU__DOT__control_unit__DOT__ControlUnit,21,0);
    VL_SIG(CPU__DOT__rom__DOT__data,31,0);
    VL_SIG(CPU__DOT__sis__DOT__sh,31,0);
    VL_SIG(CPU__DOT__sis__DOT__sb,31,0);
    VL_SIG(CPU__DOT__ram__DOT__data,31,0);
    //char	__VpadToAlign100[4];
    VL_SIG(CPU__DOT__control_unit__DOT__memory_cu[64],21,0);
    VL_SIG8(CPU__DOT__control_unit__DOT__memory_acu[64],3,0);
    VL_SIG(CPU__DOT__rom__DOT__memory[256],31,0);
    VL_SIG(CPU__DOT__registerfile__DOT__registeries[32],31,0);
    VL_SIG(CPU__DOT__ram__DOT__register[256],31,0);
    
    // LOCAL VARIABLES
    // Internals; generally not touched by application code
    VL_SIG8(__Vclklast__TOP__clk,0,0);
    //char	__VpadToAlign2605[3];
    
    // INTERNAL VARIABLES
    // Internals; generally not touched by application code
    //char	__VpadToAlign2612[4];
    VCPU__Syms*	__VlSymsp;		// Symbol table
    
    // PARAMETERS
    // Parameters marked /*verilator public*/ for use by application code
    
    // CONSTRUCTORS
  private:
    VCPU& operator= (const VCPU&);	///< Copying not allowed
    VCPU(const VCPU&);	///< Copying not allowed
  public:
    /// Construct the model; called by application code
    /// The special name  may be used to make a wrapper with a
    /// single model invisible WRT DPI scope names.
    VCPU(const char* name="TOP");
    /// Destroy the model; called (often implicitly) by application code
    ~VCPU();
    
    // USER METHODS
    
    // API METHODS
    /// Evaluate the model.  Application must call when inputs change.
    void eval();
    /// Simulation complete, run final blocks.  Application must call on completion.
    void final();
    
    // INTERNAL METHODS
  private:
    static void _eval_initial_loop(VCPU__Syms* __restrict vlSymsp);
  public:
    void __Vconfigure(VCPU__Syms* symsp, bool first);
  private:
    static QData	_change_request(VCPU__Syms* __restrict vlSymsp);
    void	_configure_coverage(VCPU__Syms* __restrict vlSymsp, bool first);
    void	_ctor_var_reset();
  public:
    static void	_eval(VCPU__Syms* __restrict vlSymsp);
    static void	_eval_initial(VCPU__Syms* __restrict vlSymsp);
    static void	_eval_settle(VCPU__Syms* __restrict vlSymsp);
    static void	_initial__TOP__1(VCPU__Syms* __restrict vlSymsp);
    static void	_initial__TOP__3(VCPU__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__2(VCPU__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__5(VCPU__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__7(VCPU__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__9(VCPU__Syms* __restrict vlSymsp);
    static void	_settle__TOP__4(VCPU__Syms* __restrict vlSymsp);
    static void	_settle__TOP__6(VCPU__Syms* __restrict vlSymsp);
    static void	_settle__TOP__8(VCPU__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
