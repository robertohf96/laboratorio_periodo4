#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <verilated.h> // Defines common routines
#include "VCPU.h"

VCPU *uut;

int main(int argc, char** argv)
{

    uut = new VCPU;
    
    uut->clk = 0;
    uut->eval();

    for(int i=0; i<50;i++){
        uut->clk = !(uut->clk);
        uut->eval();
    }

    for(int i=0; i<11; i++){
        printf("Rom:%d = %x\n",i,uut->CPU__DOT__rom__DOT__memory[i]);
    }

    printf("\n-------------------------------------------------------\n\n");

    for(int i=0; i<32;i++){
        printf("RegisterFile:%d = %x\n",i,uut->CPU__DOT__registerfile__DOT__registeries[i]);
    }

    printf("\n-------------------------------------------------------\n\n");

    for(int i=0; i<255;i++){
        printf("DataMemory:%d = %x\n",i,uut->CPU__DOT__ram__DOT__register[i]);
    }
    
	uut->final(); // Done simulating
	delete uut;

	return 0;
}