module pc(
	input clk,
	input [7:0]mux_output,
	output [7:0]address
	);
	
	reg [7:0]pc;

	always @(posedge clk)
		pc <= mux_output;

	assign address = pc;

endmodule