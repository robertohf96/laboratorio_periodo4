module pc_sel(
	input bz,
	input bnz,
	input isZero,
	output pc_sel
	);
	
	reg select;

	always @(bz or bnz)
		select <= (isZero & bz) | (~isZero & bnz);
	
	assign pc_sel = select;
	
endmodule