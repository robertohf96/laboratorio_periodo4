`include "alu_func.vh"

module factorial_fsm(
	input [2:0]current_state,
	input isZero,
	output [2:0]ra1,
	output [2:0]rd1
	output [2:0]wa,
	output [2:0]operation,
	output [2:0]next_state,
	output we

);

parameter START = 3'b000;
parameter Q1 = 3'b001;
parameter Q2 = 3'b011;
parameter Q3 = 3'b100;

always @(current_state or isZero)
begin
	case(current_state)
	begin	
		START:
			next_state = Q0;
			ra1 = 3'd1;
			ra2 = 3'dx;
			wa = 3'dx;
			we = 1'b0;
			op = 3'bx;

		Q0:
			if(isZero)
				next_state = Q1;
				ra1 = 3'd1;
				ra2 = 3'd0;
				wa = 3'dx;
				we = 1'b0;
				op = 3'bx;
			else
				next_state = Q2;
				ra1 = 3'd0;
				ra2 = 3'd1;
				wa = 3'd0;
				we = 1'b1;
				op = `ALU_MUL;

		Q1:
			next_state = Q1;
			ra1 = 3'd1;
			ra2 = 3'd0;
			wa = 3'dx;
			we = 1'b0;			
			op = 3'bx;

		Q2:
			next_state = Q3;
			ra1 = 3'd1;
			ra2 = 3'd2;
			wa = 3'd1;
			we = 1'b1;			
			op = `ALU_SUB;
		Q3:
			next_state = Q0;
			ra1 = 3'd1;
			ra2 = 3'dx;
			wa = 3'dx;
			we = 1'b0;			
			op = 3'bx;

		default:
			next_state = Q0;
			ra1 = 3'd1;
			ra2 = 3'dx;
			wa = 3'dx;
			we = 1'b0;			
			op = 3'bx;
	endcase 

end
endmodule // factorial_fsm
