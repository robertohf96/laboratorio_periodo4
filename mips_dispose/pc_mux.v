module pc_mux(
	input pc_sel,
	input [7:0]address,
	input [7:0]branch_address,
	output reg [7:0]mux_output
	);

	always @(pc_sel)
	begin
		if(pc_sel == 0)
			mux_output <= address + 1;
		else
			mux_output <= branch_address;
	end

endmodule